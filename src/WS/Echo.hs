module WS.Echo
    ( wsApp
    ) where

import qualified Data.Text                      as T
import qualified Network.WebSockets             as WS

import           Import

wsApp :: Env -> WS.ServerApp
wsApp env pending = do
    debug "Incoming websocket connection."
    WS.acceptRequest pending >>= go
  where
    debug = runAppT env . logDebugN

    go :: WS.Connection -> IO ()
    go conn = do
        msg <- WS.receiveDataMessage conn
        debug $ T.pack $ show msg
        WS.sendDataMessage conn msg
        go conn
