module App.Server where

import           Network.Wai.Handler.WebSockets as WS
import           Network.HTTP.Types.Header
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Network.Wai.Handler.WarpTLS
import           Network.Wai.HTTP2
import           Network.HTTP.Types.Status (status200)
import           Network.Wai.Middleware.RequestLogger
import qualified Database.PostgreSQL.Query as PG
import qualified Database.PostgreSQL.Simple as PG (close)
import qualified Database.Redis            as R
import           Control.Monad.Trans.Either (EitherT, left, right)
import           System.Log.FastLogger
import           Data.Pool
import qualified Network.WebSockets             as WS

import           Import
import qualified API.V1.Types    as APIv1
import qualified API.V1.Handlers as APIv1
import qualified WS.Echo         as WSEcho

initEnv :: IO Env
initEnv = Env
    <$> pure users
    <*> l
    <*> pg
    <*> r
  where
    users = pure
        [ APIv1.User 1 "Isaac" "Newton"
        , APIv1.User 2 "Albert" "Einstein"
        , APIv1.User 2 "Albert" "Einstein from the Future"
        ]

    l = newStdoutLoggerSet defaultBufSize

    pg = createPool
        (PG.connectPostgreSQL "")
        PG.close
        1 -- stripes
        10 -- timeout
        2 -- pool size

    r = R.connect R.defaultConnectInfo

testEnv :: Env -> IO Env
testEnv env = runAppT env $ do
    logDebugN "Starting app..."

    void $ redis R.ping

    PG.pgQuery ("SELECT 1" :: Text) >>= \case
        [PG.Only (1 :: Int)] -> return ()
        x -> liftIO . throwIO
                    . PostgresError
                    $ "Bad test reply:" <> show x

    pure env

startApp :: IO ()
startApp = do
    env <- testEnv =<< initEnv

    requestLogger <- mkRequestLogger def
        { outputFormat = Detailed True
        , autoFlush = True
        , destination = Logger (loggerSet env)
        }

    let h1app = requestLogger $ servantApp env
        h2app = promoteApplication h1app

    let tls = tlsSettings "etc/certificate.pem" "etc/key.pem"
        warp = defaultSettings
            & setHost "*6"
            & setPort 8080
            & setOnExceptionResponse defaultOnExceptionResponse

    runAppT env $ logDebugN "https://127.0.0.1:8080/"
    runHTTP2TLS tls warp h2app h1app

type ServerLayout =
         "api" :> "v1" :> APIv1.API
    :<|> "ws" :> "echo" :> Raw
    :<|> Raw -- static files fallback

-- Components

servantApp :: Env -> Application
servantApp env = serve (Proxy :: Proxy ServerLayout) server
  where
    server :: Server ServerLayout
    server = enter nat APIv1.handlers
        :<|> wsHandler
        :<|> rootHandler

    wsHandler :: Server Raw
    wsHandler = websocketsOr
        WS.defaultConnectionOptions
        (WSEcho.wsApp env)
        wsFallback

    wsFallback :: Application
    wsFallback _ resp = resp $
        responseLBS status200 [] "WS endpoint."

    rootHandler :: Server Raw
    rootHandler = serveDirectory "static"

    nat :: AppT :~> EitherT ServantErr IO
    nat = Nat $ \appHandlers -> do
        res <- liftIO . err $ runAppT env appHandlers
        case res of
            Right x -> right x
            Left se -> left se

    err :: IO a -> IO (Either ServantErr a)
    err io = catches (fmap Right io)
        [ Handler $ \case
            ServantError se -> pure $ Left se
            AppError msg details -> json500 msg details
            RedisError e -> json500 "Redis error" $ show e
            PostgresError e -> json500 "PostgreSQL error" e
        , Handler $ \(SomeException e) ->
            json500 "Server made a boo boo." (toJSON $ show e)
        ]

    json500 :: ToJSON d => Text -> d -> IO (Either ServantErr a)
    json500 msg details = pure $ Left err500
        { errBody = encode $ object
            [ "error" .= msg
            , "details" .= details
            ]
        , errHeaders = [(hContentType, "application/json")]
        }
