module Import.NoApp
    ( module Import
    , (&)
    ) where

import           Control.Exception   as Import
import           Control.Monad       as Import
import           Control.Monad.Trans as Import (liftIO)
import           Data.Aeson          as Import
import           Data.Aeson.TH       as Import
import           Data.Default        as Import
import           Data.Monoid         as Import
import           Data.Text.Lazy      as Import (Text)
import           Servant             as Import

(&) :: a -> (a -> b) -> b
(&) = flip ($)
