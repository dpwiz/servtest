module API.V1.Types where

import Import.NoApp

type API =
         "users" :> Get '[JSON] [User]
    :<|> "users" :> Capture "userid" Int :> Get '[JSON] User

api :: Proxy API
api = Proxy

data User = User
  { userId        :: Int
  , userFirstName :: String
  , userLastName  :: String
  } deriving (Eq, Show)

$(deriveJSON defaultOptions ''User)
